/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reparaciones;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import com.itextpdf.text.*;
import java.io.FileReader;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;

/**
 *
 * @author win10
 */
public class Cliente extends Agent{
    Ventana vnt;
    Boolean pc = false;
    Boolean cel = false;
    Boolean facturando = false;
    Boolean fase2 = false;
    ArrayList<String> servicios = new ArrayList();
    class Atender extends SimpleBehaviour{
        @Override
        public void action() {
            boolean i=true;
            System.out.println("Recepcionista comienza servicios");
            while(i){
                System.out.println(vnt.getTrue());
                if(vnt.getTrue()){
                    String mensaje = vnt.getTF1();
                    vnt.setTA1(mensaje);
                    procesaMensaje(mensaje);
                    vnt.setTrue(false);
                    vnt.setTF1("");
                }
            }
               
          
         }
        
                 @Override
        public boolean done() {
          System.out.println("Terminado");
          return true;
        }
        
        
        public void procesaMensaje(String m){
            if(m.compareToIgnoreCase("hola") ==0){
                vnt.limpia();
                vnt.setTA1("Buen dia, que desea reparar\n1:Una pc\n 2:Un celular\n");
            }
            else if(m.compareToIgnoreCase("1") ==0 && (!pc && !cel)){
                pc = true;
                vnt.setTA1("Para PC's tenemos los siguientes servicios\n"
                        + "1:Fomateo con un costo de $200\n"
                        + "2:Mantenimiento preventico con un costo de $200\n"
                        + "3:Colocar una memoria RAM de 4GB extra con un costo de $1500\n"
                        + "4:Cambiar su disco duro por otro de 1TB con un costo de $2500\n"
                        + "5:Cambio de teclado con un costo de $300");
            }
            else if(m.compareToIgnoreCase("1") ==0 && pc){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion1("1");
                pc = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("2") ==0 && pc){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion1("2");
                pc = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("3") ==0 && pc){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion1("3");
                pc = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("4") ==0 && pc){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion1("4");
                pc = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("5") ==0 && pc){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion1("5");
                pc = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("2") ==0 && (!pc && !cel)){
                cel = true;
                vnt.setTA1("Para Celulares tenemos los siguientes servicios\n"
                        + "1:Cambio de centro de carga con un costode  $200\n"
                        + "2:Cambio de display con un costo de  $600\n"
                        + "3:Desbloque de cualquier compañia con un costo de  $200\n"
                        + "4:Colocar la mica de cristal con un costo de $100\n"
                        + "5:Cambio de plug de audio con un costo de $300\n");
            }
            else if(m.compareToIgnoreCase("1") ==0 && cel){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion2("1");
                cel = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("2") ==0 && cel){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion2("2");
                cel = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("3") ==0 && cel){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion2("3");
                cel = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("4") ==0 && cel){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion2("4");
                cel = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("5") ==0 && cel){
                vnt.setTA1("Un momento nuestro mientras nuestro tecnico haces su evaluacion servicio requerido");
                conexion2("5");
                cel = false;
                vnt.setTA1("Desea solicitar otro servicio (si/no)?");
            }
            else if(m.compareToIgnoreCase("si") ==0 && !facturando){
                vnt.setTA1("Qué desea reparar?\n1:Una pc\n 2:Un celular\n");
            }
            else if(m.compareToIgnoreCase("no") == 0 && !facturando){
                facturando = true;
                vnt.setTA1("Desea facturar los servicios solicitados(si/no)?");
            }
            else if(m.compareToIgnoreCase("no") == 0 && facturando){
                facturando = false;
                fase2 = false;
                vnt.setTA1("Hasta luego");
            }
            else if(m.compareToIgnoreCase("si") == 0 && facturando){
                vnt.setTA1("Porfavor proporcione un RFC");
                fase2 = true;
            }
            else if(facturando && fase2){
                vnt.setTA1("Facturando ...");
                guardaServicios(m);
                facturando = false;
                fase2 = false;
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
                vnt.setTA1("Factura generada\nHasta luego");
                
            }
            else{
                vnt.setTA1("Lo sentimos, por el momento no contamos con ese servicio");
            }
            
        }
        
        public void guardaServicios(String rfc){
            try {
                FileWriter fw = null;
            String serie = genSerie();
            File archivo = new File("Facura"+serie+".txt");
            fw = new FileWriter(archivo);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter wr = new PrintWriter(bw);
            wr.write("FACTURA  FOLIO\t"+serie+"\n");
            wr.append("RFC del contribuyente:"+rfc+"\n");
            for(int i = 0; i< servicios.size();i++){
                wr.append(servicios.get(i) + "\n");
            }
            wr.close();
            bw.close();
                try {
                    convertTextToPDF(archivo);
                } catch (Exception e) {
                    System.err.println(e);
                }
            } catch (Exception e) {
                System.err.println(e);
            }
            
            
        }
        
        boolean convertTextToPDF(File file) throws Exception {
        BufferedReader br = null;
            try {
            Document pdfDoc = new Document(PageSize.A7);
            String output_file = file.getName().replace(".txt", ".pdf");
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(output_file)).setPdfVersion(PdfWriter.VERSION_1_7);;
            pdfDoc.open();
            Font myfont = new Font();
            myfont.setStyle(Font.BOLDITALIC);
            myfont.setSize(10);
            if (file.exists()) {
                br = new BufferedReader(new FileReader(file));
                String strLine;
                while ((strLine = br.readLine()) != null) {
                    Paragraph para = new Paragraph(strLine + "\n", myfont);
                    para.setAlignment(Element.ALIGN_JUSTIFIED);
                    pdfDoc.add(para);
                }
            } else {
                System.out.println("no such file exists!");
                return false;
            }
                pdfDoc.close();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                if(br != null)br.close();
            }
            return true;
        }
        
        public String genSerie(){
            SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            return String.valueOf(sdf.format(timestamp));
        }
        
        public void conexion2(String numero){
            try {
                    Socket socket = new Socket("192.168.0.1",5001);
                    ObjectOutputStream oss = new ObjectOutputStream(socket.getOutputStream());
                    ObjectInputStream ois = null;
                    System.out.println("Enviando peticion al tecnico ");
                    ACLMessage msj = new ACLMessage();
                    msj.setContent(numero);
                    oss.writeObject(msj);
                    ois = new ObjectInputStream(socket.getInputStream());
                    ACLMessage res =(ACLMessage) ois.readObject();
                    vnt.setTA1(res.getContent());
                    servicios.add(res.getContent());
                    ois.close();
                    oss.close();
                } catch (Exception e) {
                    System.err.println(e);
                }
        }
        
         public void conexion1(String numero){
            try {
                    Socket socket = new Socket("192.168.0.1",5000);
                    ObjectOutputStream oss = new ObjectOutputStream(socket.getOutputStream());
                    ObjectInputStream ois = null;
                    System.out.println("Enviando peticion al tecnico");
                    ACLMessage msj = new ACLMessage();
                    msj.setContent(numero);
                    oss.writeObject(msj);
                    ois = new ObjectInputStream(socket.getInputStream());
                    ACLMessage res =(ACLMessage) ois.readObject();
                    vnt.setTA1(res.getContent());
                    servicios.add(res.getContent());

                    ois.close();
                    oss.close();
                } catch (Exception e) {
                    System.err.println(e);
                }
        }
        
    }
        
    
    @Override
    protected void setup() {
        vnt=new Ventana();
        vnt.setVisible(true);
       //vnt.setTA3(" Hey 2");
        addBehaviour(new Cliente.Atender());
    }
    
    
}
